# Product Manager
 
### Pre-requisite:
 MYSQL: The application connects to mysql on localhost:3306
 
### Setup
 Change the password in application.properties to the password of the mysql
 
 ``spring.datasource.password=<to_your_root_password>
``

### Run
 After setting up the password, run the start script
 
 `` ./start``
 
 ### Tests
 
 Get products (Authenticated call):
 
 ``
 curl localhost:8080/products -u user:user
``

Get specific product details:
 ``
  curl localhost:8080/products/1 -u user:user
 ``
 
 Add a product:
 
 ``
 curl --location --request POST 'localhost:8080/products' --header 'Content-Type: application/json' --data-raw '{"name": "Product C","price":"1011"}' -u admin:admin
 ``
 
 Update a product:
 
 ``curl --location --request PUT 'localhost:8080/products/1' --header 'Content-Type: application/json' --data-raw '{"name": "Product X renamed to XXx","price":"1000"}' -u admin:admin``
 
Unauthorized call:

 ``curl localhost:8080/products``