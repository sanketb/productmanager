package com.product;



import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.math.BigDecimal;


@SpringBootApplication
public class StartProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(StartProductApplication.class, args);
    }
    @Profile("productmanager_demo")
    @Bean
    CommandLineRunner initDatabase(ProductRepository repo) {
        return args -> {
            repo.save(new Product("Product X",  new BigDecimal("10.5")));
            repo.save(new Product("Product Y", new BigDecimal("50.1")));
            repo.save(new Product("Product Z",  new BigDecimal("1000")));
        };
    }

}