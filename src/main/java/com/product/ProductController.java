package com.product;

import com.product.error.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@Validated
public class ProductController {

    @Autowired
    private ProductRepository repository;

    @GetMapping("/products")
    List<Product> findAll() {
        return repository.findAll();
    }

    // Create a product
    @PostMapping("/products")
    @ResponseStatus(HttpStatus.CREATED)
    Product createProduct(@Valid @RequestBody Product newProduct) {
        return repository.save(newProduct);
    }


    @GetMapping("/products/{id}")
    Product findProduct(@PathVariable @Min(1) Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));
    }

    @PutMapping("/products/{id}")
    Product updateProduct(@RequestBody Product newProduct, @PathVariable Long id) {

        return repository.findById(id)
                .map(x -> {
                    x.setName(newProduct.getName());

                    x.setPrice(newProduct.getPrice());
                    return repository.save(x);
                })
                .orElseGet(() -> {
                    newProduct.setId(id);
                    return repository.save(newProduct);
                });
    }   

}
